const preview = document.querySelector("#preview");
const previewScale = document.querySelector("#preview-scale");
const previewFlip = document.querySelector("#preview-flip");
const brightnessSlider = document.querySelector("#brightness");
const brightnessSliderValue = document.querySelector("#brightness-value");
const rotateSlider = document.querySelector("#rotate");
const rotateSliderValue = document.querySelector("#rotate-value");

const handleRotate = () => {
  const rotate = rotateSlider.value;
  rotateSliderValue.innerText = rotate;

  // TODO: write your code here
};

const handleBrightness = () => {
  const brightness = brightnessSlider.value;
  brightnessSliderValue.innerText = brightness;

  // TODO: write your code here
  preview.children[0].style.opacity = brightness;
};

const handleFilter = (e) => {
  const { target } = e;
  const { id: filter } = target;
  // filter: "grayscale" | "sepia" | "invert" | "hue-rotate" | "contrast" | "saturate" | "blur"
    
  const image = preview.children[0];

  // TODO: write your code here
  switch (filter) {
    case 'grayscale':
      image.style.filter = "grayscale(1)";
      break;
  
      case 'sepia':
        image.style.filter = "sepia(1)";
        break;
      
      case 'invert':
        image.style.filter = "invert(1)";
        break;
      
      case 'blur':
        image.style.filter = "blur(20px)";
        break;
      case 'saturate':
        image.style.filter = "saturate(2)";
        break;
      case 'contrast':
        image.style.filter = "contrast(2)";
        break;
      case 'hue-rotate':
        image.style.filter = "hue-rotate(90deg)";
        break;
    default:
      image.style.removeProperty('filter');
      break;
  }
};

const handleFlip = (flip) => {
  //  flip: "vertical" | "horizontal"
  const image = preview.children[0];
  console.log(image);
  // TODO: write your code here
  const hori = 1;
  const vert = 1
  if(flip === 'vertical'){
    image.style.setProperty("tranform",sc)
  }
};

const handleMouseEnter = () => {
  // TODO: write your code here
};

const handleMouseLeave = () => {
  // TODO: write your code here
};

const handleMouseMove = (e) => {
  const imageWidth = previewScale.offsetWidth;
  const imageHeight = previewScale.offsetHeight;
  const imageOffsetTop = previewScale.offsetTop;
  const imageOffsetLeft = previewScale.offsetLeft;
  const pageX = e.pageX;
  const pageY = e.pageY;

  // TODO: write your code here
};
